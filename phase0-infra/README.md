# Paso 1 - Dependency track setup

1- Iniciar el docker compose
    ```
    docker-compose up -d
    ```

2- Acceder a la UI de Dtrack en el puerto 8080. Al entrar por primera vez usaremos las credenciales `admin/admin` y nos pedirá restablecer la contraseña. ej: admin/ehu-devsecops

3- El siguiente paso consiste en generar una API Key que usaremos en el pipeline. Para ello pulsar en `Administration > Teams > Administrators` y puslsar sobre el botón + en la seccion de API Keys y copiar dicha clave

4- Subir el SBOM generado con la herramienta de Cyclonde DX:

``` 
curl -X "POST" "http://dtrack.example.com/api/v1/bom" \
     -H 'Content-Type: multipart/form-data' \
     -H "X-Api-Key: ADa1n5J0khBZg7G9seMofJy5qVibO19j " \
     -F "autoCreate=true" \
     -F "projectName=group1" \
     -F "projectVersion=v1.0" \
     -F "bom=@bom.json"
```

