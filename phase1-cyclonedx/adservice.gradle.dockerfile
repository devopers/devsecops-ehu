FROM gradle:5.6
ARG DTRACK_SERVER_IP=54.73.56.107
ARG DTARACK_PROJECT_NAME=test
ARG DTARACK_PROJECT_VERSION=v2.0
ARG DTRACK_API_KEY=Fy7cd2KzaYYxiZTDLcO36OVfmhV5d42Q

COPY ./app/insecure-microservices-demo/src/adservice /app
WORKDIR /app
RUN gradle cyclonedxBom
RUN curl -X "POST" "http://${DTRACK_SERVER_IP}:8081/api/v1/bom" \
        -H 'Content-Type: multipart/form-data' \
        -H "X-Api-Key: ${DTRACK_API_KEY} " \
        -F "autoCreate=true" \
        -F "projectName=${DTARACK_PROJECT_NAME}-adservice" \
        -F "projectVersion=${DTARACK_PROJECT_VERSION}" \
        -F "bom=@/app/build/reports/bom.json"