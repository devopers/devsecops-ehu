FROM node:17
ARG DTRACK_SERVER_IP=54.73.56.107
ARG DTARACK_PROJECT_NAME=test
ARG DTARACK_PROJECT_VERSION=v1.0
ARG DTRACK_API_KEY=Fy7cd2KzaYYxiZTDLcO36OVfmhV5d42Q

RUN npm install -g @cyclonedx/bom

COPY ./app/insecure-microservices-demo/src/paymentservice /app
WORKDIR /app
RUN npm i
RUN cyclonedx-node -o bom.json
RUN curl -X "POST" "http://${DTRACK_SERVER_IP}:8081/api/v1/bom" \
        -H 'Content-Type: multipart/form-data' \
        -H "X-Api-Key: ${DTRACK_API_KEY} " \
        -F "autoCreate=true" \
        -F "projectName=${DTARACK_PROJECT_NAME}-paymentservice" \
        -F "projectVersion=${DTARACK_PROJECT_VERSION}" \
        -F "bom=@/app/bom.json"
  