FROM golang:1.18
ARG DTRACK_SERVER_IP=54.73.56.107
ARG DTARACK_PROJECT_NAME=test
ARG DTARACK_PROJECT_VERSION=v1.0
ARG DTRACK_API_KEY=Fy7cd2KzaYYxiZTDLcO36OVfmhV5d42Q

RUN go install github.com/CycloneDX/cyclonedx-gomod/cmd/cyclonedx-gomod@latest

COPY ./app/insecure-microservices-demo/src/checkoutservice /app
WORKDIR /app
RUN cyclonedx-gomod mod -licenses -json -output bom.json .
RUN curl -X "POST" "http://${DTRACK_SERVER_IP}:8081/api/v1/bom" \
        -H 'Content-Type: multipart/form-data' \
        -H "X-Api-Key: ${DTRACK_API_KEY} " \
        -F "autoCreate=true" \
        -F "projectName=${DTARACK_PROJECT_NAME}-checkoutservice" \
        -F "projectVersion=${DTARACK_PROJECT_VERSION}" \
        -F "bom=@/app/bom.json"
  