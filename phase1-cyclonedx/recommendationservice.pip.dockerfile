FROM python:3.10
ARG DTRACK_SERVER_IP=54.73.56.107
ARG DTARACK_PROJECT_NAME=test
ARG DTARACK_PROJECT_VERSION=v1.0
ARG DTRACK_API_KEY=Fy7cd2KzaYYxiZTDLcO36OVfmhV5d42Q

RUN pip install cyclonedx-bom

COPY ./app/insecure-microservices-demo/src/recommendationservice /app
WORKDIR /app
RUN cyclonedx-bom -r --format json -o bom.json
RUN curl -X "POST" "http://${DTRACK_SERVER_IP}:8081/api/v1/bom" \
        -H 'Content-Type: multipart/form-data' \
        -H "X-Api-Key: ${DTRACK_API_KEY} " \
        -F "autoCreate=true" \
        -F "projectName=${DTARACK_PROJECT_NAME}-recommendationservice" \
        -F "projectVersion=${DTARACK_PROJECT_VERSION}" \
        -F "bom=@/app/bom.json"
  